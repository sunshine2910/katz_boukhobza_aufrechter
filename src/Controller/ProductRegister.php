<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Security\AppCustomAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductRegister extends AbstractController
{
    #[Route('/addProduct', name: 'product_register')]
    public function register(Request $request, EntityManagerInterface $entityManager): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($product);
            $entityManager->flush();
            // do anything else you need here, like send an email
            return $this->redirectToRoute('default_index');
        }

        return $this->render('registration/addProductForm.html.twig', [
            'addProductForm' => $form->createView(),
        ]);
    }
}