<?php

namespace App\Controller;

use App\Entity\Marque;
use App\Form\MarqueFormType;
use App\Repository\MarqueRepository;
use App\Repository\MovieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MarqueController extends AbstractController
{
    #[Route('/marque', name: 'marque_index')]
    public function index(MarqueRepository $marqueRepository): Response
    {
        return $this->render('marque/index.html.twig', [
            'marques' => $marqueRepository->findAll()
        ]);
    }

    #[Route('/marque/create', name: 'marque_create')]
    public function create(Request $request, EntityManagerInterface $entityManager): Response
    {
        $marque = new Marque();
        $form = $this->createForm(MarqueFormType::class, $marque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($marque);
            $entityManager->flush();
            // do anything else you need here, like send an email
            return $this->redirectToRoute('marque_index');
        }

        return $this->render('marque/marqueRegistration.html.twig', [
            'addMarqueForm' => $form->createView(),
        ]);
    }
}
