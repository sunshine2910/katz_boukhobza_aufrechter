<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    #[Route('/article', name: 'article_index')]
    public function index(): Response
    {
        return $this->render('article/index.html.twig', [
            'articles' => 'ArticleController',
        ]);
    }
    #[Route('/article/create', name: 'article')]
    public function create(Request $request, EntityManagerInterface $entityManager): Response
    {
        $article = new  Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($article);
            $entityManager->flush();
            // do anything else you need here, like send an email
            return $this->redirectToRoute('article_index');
        }

        return $this->render('article/create.html.twig', [
            'article_create' => $form->createView(),
        ]);
    }
}
