<?php
namespace App\Components;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent('design', template: 'components/design.html.twig')]
class Design
{
    public string $url;
    public string $author;

    public function mount(){
        $this->url = !isset($this->url) ? 'https://cdn.dribbble.com/users/1791433/screenshots/4268322/media/beaef1fcf72f35ac613a94b2cefe51fb.png?compress=1&resize=800x600&vertical=top' : $this->url;;
        $this->author = !isset($this->author) ? 'Cristina' : $this->author;;
    }
}
