<?php
namespace App\Components;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent('formProductButton', template: 'components/formProductButton.html.twig')]
class FormProductRegisterButton
{
    public string $url;
    public string $message;

    public function mount(){
        $this->url = !isset($this->url) ? '/marque/create' : $this->url;;
        $this->message = !isset($this->message) ? '+' : $this->message;;
    }
}
